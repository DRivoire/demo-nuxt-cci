export const state = () => ({
  lang: 'fr-fr'
})

export const mutations = {
  switchLang: (state, lang) => {
    state.lang = lang
  }
}
