import MyComponent from '../../../../slices/Banner';
import SliceZone from 'vue-slicezone'

export default {
  title: 'slices/Banner'
}


export const _Default = () => ({
  components: {
    MyComponent,
    SliceZone
  },
  methods: {
    resolve() {
      return MyComponent
    }
  },
  data() {
    return {
      mock: {"variation":"default","name":"Default","slice_type":"banner","items":[],"primary":{"title":[{"type":"heading1","text":"Enhance dynamic networks","spans":[]}],"dec":[{"type":"paragraph","text":"Deserunt ad elit ut occaecat esse tempor in aute nisi in sit quis cillum consequat magna. Do incididunt culpa laborum mollit ullamco cillum aute elit.","spans":[]}],"img":{"dimensions":{"width":900,"height":500},"alt":"Placeholder image","copyright":null,"url":"https://images.unsplash.com/photo-1596195689404-24d8a8d1c6ea?w=900&h=500&fit=crop"}},"id":"_Default"}
    }
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />'
})
_Default.storyName = 'Default'
